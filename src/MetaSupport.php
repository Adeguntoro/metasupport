<?php

namespace Adeguntoro\metasupport;

use Illuminate\Support\ServiceProvider;

class MetaSupport extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->publishes([
            __DIR__.'/config/metasupport.php' => config_path('metasupport.php')
        ]);

        $this->loadViewsFrom(__DIR__.'/resources/views', 'metasupport');
    }
}
